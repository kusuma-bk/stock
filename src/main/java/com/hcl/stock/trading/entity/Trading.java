package com.hcl.stock.trading.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Trading {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long tradingId;
	private Long userId;
	private Long stockId;
	private int numberOfStocksUserPurchased;
	private double amountOfStock;
	private LocalDate tradingDate;

}
