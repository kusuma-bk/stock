package com.hcl.stock.trading.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.stock.trading.dto.TradingRequestDto;
import com.hcl.stock.trading.dto.TradingResponseDto;
import com.hcl.stock.trading.globalexception.InsufficientBalanceException;
import com.hcl.stock.trading.globalexception.StockLimitException;
import com.hcl.stock.trading.service.TradingService;

@RequestMapping("/trading")
@RestController
public class TradingController {

	@Autowired
	TradingService tradingService;

	@PostMapping("/trade")
	public ResponseEntity<TradingResponseDto> trading(@RequestBody TradingRequestDto tradingRequestDto)
			throws StockLimitException, InsufficientBalanceException {
		TradingResponseDto tradingResponseDto = tradingService.trading(tradingRequestDto);
		return new ResponseEntity<TradingResponseDto>(tradingResponseDto, HttpStatus.OK);

	}
}
