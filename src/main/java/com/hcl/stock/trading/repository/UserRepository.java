package com.hcl.stock.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.stock.trading.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
