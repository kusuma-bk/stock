package com.hcl.stock.trading.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.hcl.stock.trading.dto.TradingRequestDto;
import com.hcl.stock.trading.dto.TradingResponseDto;
import com.hcl.stock.trading.entity.Stock;
import com.hcl.stock.trading.entity.Trading;
import com.hcl.stock.trading.entity.User;
import com.hcl.stock.trading.globalexception.InsufficientBalanceException;
import com.hcl.stock.trading.globalexception.StockLimitException;
import com.hcl.stock.trading.repository.StockRepository;
import com.hcl.stock.trading.repository.TradingRepository;
import com.hcl.stock.trading.repository.UserRepository;
import com.hcl.stock.trading.utility.TradingUtil;

@Service
public class TradingServiceImpl implements TradingService {
	@Autowired
	TradingRepository tradingRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	UserRepository userRepository;

	@Override
	public TradingResponseDto trading(TradingRequestDto tradingRequestDto)
			throws StockLimitException, InsufficientBalanceException {
		TradingResponseDto tradingResponseDto = new TradingResponseDto();
		Trading trading = new Trading();

		Stock stock = new Stock();
		User user = new User();
		

		Optional<Stock> stock1 = stockRepository.findById(tradingRequestDto.getStockId());
		if (stock1.isPresent()) {
			stock = stock1.get();
		}
		Optional<User> user1 = userRepository.findById(tradingRequestDto.getUserId());
		if (user1.isPresent()) {
			user = user1.get();
		}

		double valuePerStockWithoutCommission = stock.getValuePerStock();// 50
		double comissionOfOneStock = valuePerStockWithoutCommission * 0.12;// 50*0.12=6
		double costOfOneStockWithCOmmission = valuePerStockWithoutCommission + comissionOfOneStock;// 50+6=56
		double totalCostOfStocksPurchasedWithCommission = costOfOneStockWithCOmmission
				* tradingRequestDto.getNumberOfStocksUserPurchased();// 56*5=280
		System.out.println("number of stocks in trading  ==== "+trading.getNumberOfStocksUserPurchased());

		if ((trading.getNumberOfStocksUserPurchased() > 500)
				&& (user.getUserAmount() < totalCostOfStocksPurchasedWithCommission)) {
			throw new StockLimitException(TradingUtil.TRADING_LIMIT_STATUS);
		} else {

			System.out.println("ui output=      " + tradingRequestDto.getNumberOfStocksUserPurchased());
			int numberOfStocksPurchased = trading.getNumberOfStocksUserPurchased()
					+ tradingRequestDto.getNumberOfStocksUserPurchased();
			System.out.println("number of stocks =====" + numberOfStocksPurchased);
			trading.setNumberOfStocksUserPurchased(numberOfStocksPurchased);
			trading.setStockId(tradingRequestDto.getStockId());
			trading.setAmountOfStock(totalCostOfStocksPurchasedWithCommission);
			trading.setTradingDate(LocalDate.now());
			trading.setUserId(tradingRequestDto.getUserId());

			tradingRepository.save(trading);

			if ((tradingRequestDto.getStockId() == stock.getStockId())
					&& tradingRequestDto.getUserId() == user.getUserId()) {
				int numberOfstocks = stock.getNumberOfStocks() - tradingRequestDto.getNumberOfStocksUserPurchased();
				stock.setNumberOfStocks(numberOfstocks);
				stockRepository.save(stock);

				double balanceOfUserAccount = user.getUserAmount() - totalCostOfStocksPurchasedWithCommission;
				user.setUserAmount(balanceOfUserAccount);
				userRepository.save(user);
			}

			tradingResponseDto.setMessage(TradingUtil.TRADING_STATUS);
			tradingResponseDto.setStatusCode(HttpStatus.OK.value());

		}

		return tradingResponseDto;
	}

}
