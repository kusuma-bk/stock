package com.hcl.stock.trading.service;

import com.hcl.stock.trading.dto.TradingRequestDto;
import com.hcl.stock.trading.dto.TradingResponseDto;
import com.hcl.stock.trading.globalexception.InsufficientBalanceException;
import com.hcl.stock.trading.globalexception.StockLimitException;

public interface TradingService {
	public TradingResponseDto trading(TradingRequestDto tradingRequestDto)
			throws StockLimitException, InsufficientBalanceException;

}
