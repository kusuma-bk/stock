package com.hcl.stock.trading.globalexception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.hcl.stock.trading.utility.TradingUtil;

@ControllerAdvice
public class GloblaExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(InsufficientBalanceException.class)
	public ResponseEntity<ErrorResponse> insufficientBalance(InsufficientBalanceException ex) {
		ErrorResponse errorResponse = new ErrorResponse(ex.getMessage(), TradingUtil.INSUFFICIENT_BALANCE_STATUS);
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(StockLimitException.class)
	public ResponseEntity<ErrorResponse> stockLimitException(StockLimitException ex) {
		ErrorResponse errorResponse = new ErrorResponse(ex.getMessage(), TradingUtil.TRADING_LIMIT_STATUS);
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
	}

}
