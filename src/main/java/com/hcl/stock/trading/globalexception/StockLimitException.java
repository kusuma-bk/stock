package com.hcl.stock.trading.globalexception;

public class StockLimitException extends Exception {
	private static final long serialVersionUID = 1L;

	public StockLimitException(String message) {
		super(message);
	}

}
