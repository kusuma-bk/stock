package com.hcl.stock.trading.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TradingRequestDto {
	private Long userId;
	private Long stockId;
	private int numberOfStocksUserPurchased;
	private double amountOfStock;
}
